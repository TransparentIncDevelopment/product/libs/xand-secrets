# xand-secrets

This repo houses a core trait, `SecretKeyValueStore`, which provides a simple
interface for reading secrets from — and checking the health of — a centralized
secret store. The exposed API is intended to focus on the commonality between
various implementations; secrets are indexed by a single string key.

This interface _does not_ facilitate populating or updating secrets within the
store; its administration is left up to external tooling.

## Intended structure and usage

The secret store is intended to securely hold API tokens, client certificates,
or other similar keys which are intended to be kept private. The typical
use-case is interaction with a 3rd-party, although this concept could also be
applied to interactions between two TPFS applications.

A consumer is expected to have available any authentication needed to make
requests to their secret store of choice, along with the relevant connection
details. They first use a client (implementing `SecretKeyValueStore`) to
retrieve secrets from the store, and then make a request to the external
service with the secrets that were returned. The intent is that the hosting
application _does not_ store or cache the returned secrets after making the
upstream request.

The below diagram illustrates this process, including a hypothetical Kubernetes
deployment and its role in the secret store request. This deployment pattern is
_not_ required for use of the crate, but almost any instance of a host app will
need secret store credentials made available by some means.

<img src="https://gitlab.com/TransparentIncDevelopment/product/libs/xand-secrets/uploads/952cc53c33771c66fc770c6f2259a313/secret_store_data_flow.png" width="650px" />

[Editable source](https://transparentfinancialsystems-my.sharepoint.com/:p:/g/personal/kaelin_laundry_tpfs_io/EadDBllXWYFPnBBpfTXdLnoB8RUiTPhQyL1sjd7WjMvDGg?e=DO81EC)

## Usage

All adapters are in their own isolated crates, and there is no central API for
creating them. Typically, an application will expose the secret store choice
to the user and/or application administrator as a configuration parameter.

For example, a deserialized configuration input could look like this:

```rust
pub enum SecretStoreConfig {
    Vault(VaultConfiguration),
    KeyVault(KeyVaultConfiguration),
    LocalFile(LocalFileSecretStoreConfiguration),
}
```

As of writing, existing adapters all declare a `*Configuration` struct to be
passed as an argument upon creation, but this is not inherently required.

Once a `SecretStoreConfig` is loaded, the application can create the appropriate
store using each crate's individual API:

```rust
match &configuration.secret_store {
    SecretStoreConfig::Vault(config) => Ok(Arc::new(
        VaultSecretKeyValueStore::create_from_config(config.clone())?,
    )),
    SecretStoreConfig::KeyVault(config) => Ok(Arc::new(
        KeyVaultSecretKeyValueStore::create_from_config(config.clone()),
    )),
    SecretStoreConfig::LocalFile(config) => Ok(Arc::new(
        LocalFileSecretKeyValueStore::create_from_config(config.clone()),
    )),
}
```

## Available adapters

- Hashicorp Vault: https://gitlab.com/TransparentIncDevelopment/product/libs/xand-secrets-vault
- Azure Key Vault: https://gitlab.com/TransparentIncDevelopment/product/libs/xand-secrets-key-vault
- Local, unencrypted file: https://gitlab.com/TransparentIncDevelopment/product/libs/xand-secrets-local-file
    - The local file store is intended only as a stopgap, and should be avoided
      if at all possible.