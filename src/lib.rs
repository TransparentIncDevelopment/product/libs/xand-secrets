#![forbid(unsafe_code)]

use async_trait::async_trait;

pub use secrecy::{ExposeSecret, Secret};
use thiserror::Error;

use std::error::Error as StdError;

#[derive(Debug, Error)]
pub enum ReadSecretError {
    #[error("the key \"{key}\" was not found within the secret store")]
    KeyNotFound { key: String },
    #[error("authentication with the secret store failed. {internal_error}")]
    Authentication {
        #[source]
        internal_error: Box<dyn StdError + Send + Sync>,
    },
    #[error("a request to the secret store returned an unhandled error. {internal_error}")]
    Request {
        #[source]
        internal_error: Box<dyn StdError + Send + Sync>,
    },
}

#[derive(Debug, Error)]
pub enum CheckHealthError {
    /// We reached the remote endpoint but failed to authenticate
    #[error("authentication with the secret store failed. {internal_error}")]
    Authentication {
        #[source]
        internal_error: Box<dyn StdError + Send + Sync>,
    },
    /// We reached the remote endpoint and it indicated that it was unhealthy
    #[error("the remote endpoint signalled an internal health issue. {internal_error}")]
    RemoteInternal {
        #[source]
        internal_error: Box<dyn StdError + Send + Sync>,
    },
    /// We could not reach the remote endpoint.
    #[error("remote endpoint unreachable. {internal_error}")]
    Unreachable {
        #[source]
        internal_error: Box<dyn StdError + Send + Sync>,
    },
}

#[async_trait]
pub trait SecretKeyValueStore: Send + Sync {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError>;

    async fn check_health(&self) -> Result<(), CheckHealthError>;
}
